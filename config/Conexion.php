<?php 
$host ='C:\xampp73\htdocs\Reportes\DB\SAE70EMPRE01.FDB';
$usuario = 'SYSDBA';
$password = 'masterkey';
$conexion = ibase_connect($host, $usuario, $password);

//Si tenemos un posible error en la conexión lo mostramos
if (ibase_errcode())
{
	printf("Falló conexión a la base de datos: %s\n",ibase_errmsg());
	exit();
}

if (!function_exists('ejecutarConsulta'))
{
	function ejecutarConsulta($sql)
	{
		global $conexion;
		$query = ibase_query($conexion, $sql);
		return $query;
	}

	function ejecutarConsultaSimpleFila($sql)
	{
		global $conexion;
		$query = ibase_query($conexion, $sql);		
		$row = ibase_fetch_assoc($query);
		return $row;
	}
}
?>
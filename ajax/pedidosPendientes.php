<?php
require_once '../modelos/PedidosPendientes.php';

$PedidosPendientes = new PedidosPendientes();

$cliente = isset($_POST["cliente"])?htmlspecialchars(trim($_POST["cliente"])):"";

switch ($_GET['op']) {
    case 'listar':
        $response = $PedidosPendientes->listar($cliente);
        $pedidos = array();

        while ($reg=ibase_fetch_object($response)) {
            $pedidos[] = array(
                "0" => utf8_encode($reg->clave),
                "1" => utf8_encode($reg->descr),
                "2" => utf8_encode($reg->departament),
                "3" => date_format(date_create($reg->fecha), 'd-M-y'),
                "4" => intval($reg->stock)
            );
        }

        $fechaYCantidadKits = $PedidosPendientes->fechaYCantidadKits($cliente);
        $clavesKits = array();
        
        while ($reg1=ibase_fetch_object($fechaYCantidadKits)) {
            $clavesKits[] = array(
                "clave" => utf8_encode($reg1->clave),
                "menor_5" => intval($reg1->col_1),
                "menor_12" => intval($reg1->col_2),
                "mayor_12" => intval($reg1->col_3)
            );
        }

        $fechaYCantidad = $PedidosPendientes->fechaYCantidad($cliente);
        $claves = array();
        
        while ($reg2=ibase_fetch_object($fechaYCantidad)) {
            $claves[] = array(
                "clave" => utf8_encode($reg2->clave),
                "menor_5" => intval($reg2->col_1),
                "menor_12" => intval($reg2->col_2),
                "mayor_12" => intval($reg2->col_3)
            );
        }

        for ($i=0; $i < count($clavesKits); $i++) { 
            array_push($claves, $clavesKits[$i]);
        }

        $ids_claves = [];
        foreach ($claves as $arr_clave) {
            $id_clave = $arr_clave["clave"];
            if (! in_array($id_clave, $ids_claves)) {
                $ids_claves[] = $id_clave;
            }
        }

        $claves_unique = [];
        foreach ($ids_claves as $unique_id) {
            $temp     = [];
            $quantity = 0;
            foreach ($claves as $arr_clave) {
                $id = $arr_clave["clave"];

                if ($id === $unique_id) {
                    $temp[] = $arr_clave;
                }
            }

            $clave = $temp[0];


            $clave["menor_5"] = 0;
            $clave["menor_12"] = 0;
            $clave["mayor_12"] = 0;
            foreach ($temp as $clave_temp) {
                $clave["menor_5"] = $clave["menor_5"] + $clave_temp["menor_5"];
                $clave["menor_12"] = $clave["menor_12"] + $clave_temp["menor_12"];
                $clave["mayor_12"] = $clave["mayor_12"] + $clave_temp["mayor_12"];
            }
            $claves_unique[] = $clave;
        }

        $data = array();
        for ($i=0; $i < count($pedidos) ; $i++) { 
            $key_clave = array_search($pedidos[$i]["0"], array_column($claves_unique, 'clave'));
            $total = $pedidos[$i]['4'] - ($claves_unique[$key_clave]["menor_5"] + $claves_unique[$key_clave]["menor_12"] + $claves_unique[$key_clave]["mayor_12"]);
            if ($total < 0) {
                $data[] = array(
                    '0' => $pedidos[$i]["0"],
                    '1' => $pedidos[$i]["1"],
                    '2' => $pedidos[$i]["2"],
                    '3' => $pedidos[$i]["3"],
                    '4' => $claves_unique[$key_clave]["menor_5"],
                    '5' => $claves_unique[$key_clave]["menor_12"],
                    '6' => $claves_unique[$key_clave]["mayor_12"],
                    '7' => $claves_unique[$key_clave]["menor_5"] + $claves_unique[$key_clave]["menor_12"] + $claves_unique[$key_clave]["mayor_12"],
                    '8' => $pedidos[$i]['4'],
                    '9' => $total
                );
            }
        }
        $groupDepartament = $PedidosPendientes->groupArray($data, "2");

        echo json_encode($groupDepartament);
        break;
    case 'listarClientes':
        $response = $PedidosPendientes->listarClientes();
        $data = array();

        while ($reg=ibase_fetch_object($response)) {
            $data[] = array(
                '0' => utf8_encode($reg->CLAVE),
                '1' => utf8_encode($reg->NOMBRE)
            );
        }

        echo json_encode($data);
        break;
    case 'buscarCliente':
        $response = $PedidosPendientes->buscarCliente($cliente);
        $data = array();

        while ($reg=ibase_fetch_object($response)) {
            $data[] = array(
                '0' => utf8_encode($reg->CLAVE),
                '1' => utf8_encode($reg->NOMBRE)
            );
        }

        echo json_encode($data);
        break;
    default:
        # code...
        break;
}
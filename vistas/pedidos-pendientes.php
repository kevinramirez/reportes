<!DOCTYPE html>
<html lang="en">
<?php date_default_timezone_set("America/Mexico_City"); ?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../public/assets/css/print.css">
    <title>Pedidos Pendientes</title>
</head>
<body>
    <div class="page-header" style="text-align: center">
        <h3 style="margin:0;">COMERCIAL SELECT, S.A. de C.V.</h3>
        <h4 style="margin:0;">Explosión de Pedidos Pendientes de entrega</h4>
        <h5 style="color: red; margin:0;">( Sólo negativos ) (Ordenado por fecha pedido)</h5>
        <p style="font-weight: bold; text-align: right; margin-top:0;font-size:13px;">Cierre: <?= date('d-M-Y'); ?></p>
    </div>

    <div class="page-footer">
        <table style="width:100%; font-size:10px">
            <tr>
                <td>[ Rep_backorder ]</td>
                <td>SYSTEMS-01 # ARES</td>
                <td>( *=Descontinuado Fecha Pdo.=Fecha más antigua)</td>
                <td>[ Sistema Administrativo Areslux ]</td>
            </tr>
        </table>
    </div>

    <table style="width: 100%;">
        <thead>
            <tr>
                <td>
                    <div class="page-header-space"></div>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="page" id="content"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tablatotal" style="width:100%; font-weight: bold;">
                        <tr>
                            <td style="width: 10%;"></td>
                            <td style="width:30%;"></td>
                            <td style="width: 10%;"></td>
                            <td style="width: 10%;"></td>
                            <td style="width: 12%;"></td>
                            <td>Total: </td>
                            <td id="mostrar_total_col"></td>
                            <td style="width: 10%;"></td>
                            <td id="mostrar_total_negativos"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <div class="page-footer-space"></div>
                </td>
            </tr>
        </tfoot>
    </table>
<script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="scripts/pedidosPendientes.js"></script>
</body>
</html>
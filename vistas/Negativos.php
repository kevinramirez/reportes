<?php require "header.php"; ?>

<div style="min-height: 96vh;">
<div class="container" style="font-family:  courier, Arial, Helvetica, sans-serif;">
    <div style="height: 70px;"></div>
    <form id="formulario" method="POST" class="d-print-none">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="selectCliente">Nombre Cliente:</label>
                <select name="selectCliente" id="selectCliente" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar Cliente...">
                </select>
            </div>
            <div class="form-group col-md-6">
                <button type="button" class="btn btn-primary mt-4" onclick="imprimir()">Generar</button>
            </div>
        </div>
    </form>
</div> <!-- .container -->
</div>
<?php require "footer.php"; ?>
<script src="scripts/negativos.js"></script>
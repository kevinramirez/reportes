  <!-- Footer -->
  <footer class="py-1 bg-dark d-print-none">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Areslux 2020</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="../public/assets/js/vendor/jquery.min.js"></script>
  <script src="../public/assets/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="../public/assets/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="../public/assets/js/scrolling-nav.js"></script>

  <!-- Select2 Jquery -->
  <script src="../public/assets/Select2/select2.min.js"></script>

</body>

</html>
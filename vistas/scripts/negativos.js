function init() {
    $('.form-control-chosen').select2({
        language: "es",
        width: '100%',
        allowClear: true,
    });
    listarClientes();
}

function imprimir() {
    var cliente = $("#selectCliente").val();
    window.open("pedidos-pendientes.php?cliente="+cliente+"","_reporte","width=1300px,height=700px,top=0px,left=0px");
}

function pulsarenter(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13 && tecla != 32);
}

function listarClientes() {
    $.post("../ajax/pedidosPendientes.php?op=listarClientes",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "<option value=''>General - Todo</option>";
            for (let i = 0; i < data.length; i++) {  
                select += `<option value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#selectCliente").append(select);
            $("#selectCliente").trigger("chosen:updated");
        }
    );
}

init();
var codigoCliente;
var nombreCliente;

function init() {
    var cliente =  getParameterByName('cliente');
    imprimirpag(cliente);

    /* if (window.print) {
        setTimeout(function(){window.print();window.close();},500); // 500ms = 0.5s - 1000 = 1s
    } */
}

function imprimirpag(cliente) {
    $.post("../ajax/pedidosPendientes.php?op=listar", {cliente: cliente},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            buscarCliente(cliente);
            var html = "";
            html += `<h2 id="datosCliente"></h2>`;
            for (let i = 0; i < data.length; i++) {
                html += `<h3 style="color: #0080ff;">Depto.: ${data[i]['2']}</h3>`;
                html += `<table class="tabla" style="width: 100%; border-collapse: collapse;">`;
                html += `<thead>`;
                html += `<tr style="text-align:left;">`;
                html += `<th style="width: 8%;">Clave</td>`;
                html += `<th style="width=30%;">Descripcion</th>`;
                html += `<th>Fecha Pdo.</th>`;
                html += `<th>menor o = a 5</th>`;
                html += `<th>menor o = a 12</th>`;
                html += `<th>mayor a 12</th>`;
                html += `<th>A Total</th>`;
                html += `<th>B Stock</th>`;
                html += `<th>B - A</th>`;
                html += `</tr>`;
                html += `</thead>`;
                html += `<tbody>`;
                for (let j = 0; j < data[i]['groupeddata'].length; j++) {
                    console.log(data[i]['groupeddata'][j][9]);
                    html += `<tr style="border-top: 1px solid #000; border-bottom: 1px solid #000;">`;
                    html += `<td style="width: 8%;">${data[i]['groupeddata'][j][0]}</td>`;
                    html += `<td style="width:30%">${data[i]['groupeddata'][j][1]}</td>`;
                    html += `<td>${data[i]['groupeddata'][j][3]}</td>`;
                    html += `<td class="sumar_menor_5_${i}">${data[i]['groupeddata'][j][4]}</td>`;
                    html += `<td class="sumar_menor_12_${i}">${data[i]['groupeddata'][j][5]}</td>`;
                    html += `<td class="sumar_mayor_12_${i}">${data[i]['groupeddata'][j][6]}</td>`;
                    html += `<td class="sumar_a_total_${i} sumcol">${data[i]['groupeddata'][j][7]}</td>`;
                    html += `<td class="sumar_b_stock_${i}">${data[i]['groupeddata'][j][8]}</td>`;
                    html += `<td class="sumar_b_a_${i} sumnegativos">${data[i]['groupeddata'][j][9]}</td>`;
                    html += `</tr>`;
                }
                html += `</tbody>`;
                html += `<tfoot style="text-align:left;">`;
                html += `<th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th class="total_menor_5_${i}"></th><th class="total_menor_12_${i}"></th><th class="total_mayor_12_${i}"></th><th class="total_a_total_${i}"></th><th class="total_b_stock_${i}"></th><th class="total_b_a_${i}"></th>`;
                html += `</tfoot>`;
                html += `</table>`;
                setTimeout(function(){totalcolumnas(i);}, 500);
            }
            $("#content").html(html);
            sumaTotalColumnas();
        }
    );
}

function totalcolumnas(pos) {
    var total_menor_5=0;
    $('.sumar_menor_5_'+pos).each(function() {  
        total_menor_5 += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $('.total_menor_5_'+pos).html(total_menor_5.toFixed(0));
    
    var total_menor_12=0;
    $('.sumar_menor_12_'+pos).each(function() {  
        total_menor_12 += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $('.total_menor_12_'+pos).html(total_menor_12.toFixed(0));

    var total_mayor_12=0;
    $('.sumar_mayor_12_'+pos).each(function() {  
        total_mayor_12 += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $('.total_mayor_12_'+pos).html(total_mayor_12.toFixed(0));
    
    var total_a_total=0;
    $('.sumar_a_total_'+pos).each(function() {  
        total_a_total += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $('.total_a_total_'+pos).html(total_a_total.toFixed(0));
    
    var total_b_stock=0;
    $('.sumar_b_stock_'+pos).each(function() {  
        total_b_stock += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $('.total_b_stock_'+pos).html(total_b_stock.toFixed(0));

    var total_b_a=0;
    $('.sumar_b_a_'+pos).each(function() {  
        total_b_a += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $('.total_b_a_'+pos).html(total_b_a.toFixed(0));
}

function sumaTotalColumnas() {
    var sumtotal = 0;
    $(".sumcol").each(function () { 
         sumtotal += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $("#mostrar_total_col").html(sumtotal);
	
	var totalnegativos = 0;
    $(".sumnegativos").each(function () { 
         totalnegativos += parseFloat($(this).text().replace(/,/g, ''), 10);
    });
    $("#mostrar_total_negativos").html(totalnegativos);
}

function getParameterByName(name)
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function buscarCliente(cliente) {
    $.post("../ajax/pedidosPendientes.php?op=buscarCliente", {cliente: cliente},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            $("#datosCliente").html('Cliente: ' + data[0][0] + ' - ' + data[0][1]);
        }
    );
}

init();
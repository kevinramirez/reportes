<?php
    require "../config/Conexion.php";

    class PedidosPendientes
    {
        public function __construct()
        {
            
        }

        public function listar($cliente)
        {
            $sqlCliente = !empty($cliente) ? ' AND TRIM(FACTP01.CVE_CLPV) = '.'\''.$cliente.'\'' : '';
            $sql = 'SELECT
                INVE01.CVE_ART "clave",
                INVE01.DESCR "descr",
                INVE_CLIB01.CAMPLIB9 "departament",
                MIN(FACTP01.FECHA_DOC) "fecha",
                MIN(MULT01.EXIST) "stock"
            FROM PAR_FACTP01
            INNER JOIN INVE01 ON PAR_FACTP01.cve_art = INVE01.cve_art
            INNER JOIN FACTP01 ON PAR_FACTP01.CVE_DOC = FACTP01.CVE_DOC
            INNER JOIN INVE_CLIB01 ON PAR_FACTP01.cve_art = INVE_CLIB01.CVE_PROD
            INNER JOIN MULT01 ON INVE01.CVE_ART = MULT01.CVE_ART
            WHERE FACTP01.STATUS != \'C\'
            AND MULT01.CVE_ALM = \'1\'
            AND INVE01.STATUS = \'A\'
            AND  INVE01.TIPO_ELE = \'P\'
            '.$sqlCliente.'
            GROUP BY INVE01.CVE_ART, INVE01.DESCR, INVE_CLIB01.CAMPLIB9
            ORDER BY INVE_CLIB01.CAMPLIB9 ASC;';

            return ejecutarConsulta($sql);
        }

        public function fechaYCantidad($cliente)
        {
            $sqlCliente = !empty($cliente) ? ' AND TRIM(FACTP01.CVE_CLPV) = '.'\''.$cliente.'\'' : '';
            $sql = 'SELECT
                INVE01.CVE_ART "clave",
                FACTP01.FECHA_DOC "fecha",
                IIF(DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE)<= 5, CAST(SUM(PAR_FACTP01.PXS) as integer), \'\') "col_1",
                IIF(DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE) > 5 AND DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE) <= 12, CAST(SUM(PAR_FACTP01.PXS) as integer), \'\') "col_2",
                IIF(DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE) > 12, CAST(SUM(PAR_FACTP01.PXS) as integer), \'\') "col_3",
                SUM(PAR_FACTP01.PXS) "cant"
            FROM PAR_FACTP01
            INNER JOIN INVE01 ON PAR_FACTP01.cve_art = INVE01.cve_art
            INNER JOIN FACTP01 ON PAR_FACTP01.CVE_DOC = FACTP01.CVE_DOC
            WHERE FACTP01.STATUS != \'C\'
            AND INVE01.STATUS = \'A\'
            AND  INVE01.TIPO_ELE = \'P\'
            '.$sqlCliente.'
            GROUP BY INVE01.CVE_ART, FACTP01.FECHA_DOC;';

            return ejecutarConsulta($sql);
        }

        public function fechaYCantidadKits($cliente)
        {
            $sqlCliente = !empty($cliente) ? ' AND TRIM(FACTP01.CVE_CLPV) = '.'\''.$cliente.'\'' : '';
            $sql = 'SELECT
                KITS01.CVE_PROD "clave",
                FACTP01.FECHA_DOC "fecha",
                IIF(DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE)<= 5, CAST((SUM(PAR_FACTP01.PXS) * SUM(KITS01.CANTIDAD)) as integer), \'\') "col_1",
                IIF(DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE) > 5 AND DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE) <= 12, CAST((SUM(PAR_FACTP01.PXS) * SUM(KITS01.CANTIDAD)) as integer), \'\') "col_2",
                IIF(DATEDIFF(DAY, FACTP01.FECHA_DOC, CURRENT_DATE) > 12, CAST((SUM(PAR_FACTP01.PXS) * SUM(KITS01.CANTIDAD)) as integer), \'\') "col_3",
                (SUM(PAR_FACTP01.PXS) * SUM(KITS01.CANTIDAD)) "cant"
            FROM
                KITS01
            INNER JOIN INVE01 ON KITS01.CVE_PROD = INVE01.CVE_ART
            INNER JOIN PAR_FACTP01 ON PAR_FACTP01.CVE_ART = KITS01.CVE_ART 
            INNER JOIN FACTP01 ON PAR_FACTP01.CVE_DOC = FACTP01.CVE_DOC
            WHERE FACTP01.STATUS != \'C\'
            AND INVE01.STATUS = \'A\'
            AND  INVE01.TIPO_ELE = \'K\'
            '.$sqlCliente.'
            GROUP BY KITS01.CVE_PROD, FACTP01.FECHA_DOC;';
         
            return ejecutarConsulta($sql);
        }

        function groupArray($array,$groupkey)
        {
            if (count($array)>0)
            {
                $keys = array_keys($array[0]);
                $removekey = array_search($groupkey, $keys);		if ($removekey===false)
                    return array("Clave \"$groupkey\" no existe");
                else
                    unset($keys[$removekey]);
                $groupcriteria = array();
                $return=array();
                foreach($array as $value)
                {
                    $item=null;
                    foreach ($keys as $key)
                    {
                        $item[$key] = $value[$key];
                    }
                    $busca = array_search($value[$groupkey], $groupcriteria);
                    if ($busca === false)
                    {
                        $groupcriteria[]=$value[$groupkey];
                        $return[]=array($groupkey=>$value[$groupkey],'groupeddata'=>array());
                        $busca=count($return)-1;
                    }
                    $return[$busca]['groupeddata'][]=$item;
                }
                return $return;
            }
            else
                return array();
        }

        public function listarClientes()
        {
            $sql = 'SELECT TRIM("CLAVE") "CLAVE", "NOMBRE" FROM "CLIE01" WHERE "STATUS" = \'A\' ORDER BY "CLAVE";';

            return ejecutarConsulta($sql);
        }

        public function buscarCliente($cliente)
        {
            $sql = 'SELECT TRIM("CLAVE") "CLAVE", "NOMBRE" FROM "CLIE01" WHERE "STATUS" = \'A\' AND TRIM("CLAVE") = \''.$cliente.'\' ORDER BY "CLAVE";';

            return ejecutarConsulta($sql);
        }
    }
?>